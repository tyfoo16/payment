import 'package:bjak/blocs/cubit/user_credit_cards_cubit.dart';
import 'package:bjak/dataProviders/exceptions.dart';
import 'package:bjak/models/credit_card.dart';
import 'package:mocktail/mocktail.dart';
import 'package:http/http.dart' as http;
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';

class MockCardCubit extends MockCubit<UserCreditCardsState>
    implements UserCreditCardsCubit {}

class MockCard with Fake implements CreditCard {
  final String _name;

  MockCard(this._name);

  @override
  String get name => this._name;
}

void main() {
  setUpAll(() {
    registerFallbackValue<UserCreditCardsState>(UserCreditCardsState.initial());
  });

  test("Test Fetch Success with 3 cards", () async {
    final cardBloc = MockCardCubit();

    final mockList = [
      MockCard("Michael"),
      MockCard("Sam"),
      MockCard("Noah"),
    ];
// Stub the state stream
    whenListen(
      cardBloc,
      Stream.fromIterable([
        UserCreditCardsState.loading(),
        UserCreditCardsState.fetchSuccess(mockList),
      ]),
      initialState: UserCreditCardsState.initial(),
    );

// Assert that the initial state is correct.
    expect(cardBloc.state, equals(UserCreditCardsState.initial()));
    await expectLater(
      cardBloc.stream,
      emitsInOrder([
        UserCreditCardsState.loading(),
        UserCreditCardsState.fetchSuccess(mockList),
      ]),
    );
// Assert that the current state is in sync with the stubbed stream.
    expect(cardBloc.state, equals(UserCreditCardsState.fetchSuccess(mockList)));
  });

  test("Test Fetch failed with Unauthorized error", () async {
    final cardBloc = MockCardCubit();

// Stub the state stream
    whenListen(
      cardBloc,
      Stream.fromIterable([
        UserCreditCardsState.loading(),
        UserCreditCardsState.fetchFailed(UnauthorizedException().toString()),
      ]),
      initialState: UserCreditCardsState.initial(),
    );

// Assert that the initial state is correct.
    expect(cardBloc.state, equals(UserCreditCardsState.initial()));
    await expectLater(
      cardBloc.stream,
      emitsInOrder([
        UserCreditCardsState.loading(),
        UserCreditCardsState.fetchFailed(UnauthorizedException().toString()),
      ]),
    );
// Assert that the current state is in sync with the stubbed stream.
    expect(
        cardBloc.state,
        equals(
          UserCreditCardsState.fetchFailed(UnauthorizedException().toString()),
        ));
  });
}
