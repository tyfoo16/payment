import 'package:bjak/dataProviders/apiProvider.dart';
import 'package:bjak/models/credit_card.dart';
import 'package:test/test.dart';
import 'package:http/http.dart' as http;

void main() {
  test("Test api call success with correct type.", () async {
    final client = new http.Client();
    final apiProvider = ApiProvider(client);

    expect(await apiProvider.fetchCreditCard(), isA<List<CreditCard>>());
  });
}
