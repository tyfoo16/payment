class UnauthorizedException implements Exception {
  @override
  String toString() {
    return "Unauthorized";
  }
}

class NotFoundException implements Exception {
  @override
  String toString() {
    return "404 Not Found";
  }
}

class InternalServerErrorException implements Exception {
  @override
  String toString() {
    return "Internal Server Error";
  }
}

class BadRequestException implements Exception {
  @override
  String toString() {
    return "Bad Request";
  }
}

class NetworkException implements Exception {
  @override
  String toString() {
    return "Network Exception";
  }
}

class GeneralException implements Exception {
  final String message;

  GeneralException(this.message);

  @override
  String toString() {
    return this.message;
  }
}
