import 'dart:convert';
import 'dart:io';

import 'package:bjak/models/credit_card.dart';
import 'package:http/http.dart' as http;

import 'exceptions.dart';

class ApiProvider {
  final http.Client client;

  ApiProvider(this.client);

  http.Response returnOrThrow(http.Response response) {
    print("Status Code: ${response.statusCode}");
    final status = response.statusCode;
    switch (status) {
      case 200:
        return response;
      case 400:
        throw BadRequestException();
      case 401:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException();
      case 500:
        throw InternalServerErrorException();
      default:
        throw GeneralException("Request failed: $status");
    }
  }

  Future<http.Response> _get(String url, Map<String, String> headers) async {
    var responseJson;
    try {
      final uri = Uri.parse(url);
      final response = await client.get(uri, headers: headers);
      responseJson = returnOrThrow(response);
      return responseJson;
    } on SocketException {
      throw GeneralException('No Internet connection');
    }
  }

  Future<List<CreditCard>> fetchCreditCard() async {
    try {
      final res = await _get(
        "https://api.stripe.com/v1/customers/cus_K94gx7IB6UcJLB/sources",
        {
          "Authorization": "Bearer " +
              "sk_test_51JUm5GEV8wtBYZgiCVBEDRysgy1U3GngTMKppIOxr4uCRIdDi0VFt9demarPgNJF4kVjKaMLphvIkaRB6QFhG8RT00zwNrUs4N",
        },
      );

      final js = jsonDecode(res.body);
      final list = (js["data"] as List<dynamic>)
          .map((e) => CreditCard.fromJson(e))
          .toList();

      return list;
    } catch (error) {
      throw error;
    }
  }
}
