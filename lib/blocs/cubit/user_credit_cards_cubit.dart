import 'package:bjak/dataProviders/apiProvider.dart';
import 'package:bjak/models/credit_card.dart';
import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:http/http.dart' as http;

part 'user_credit_cards_state.dart';
part 'user_credit_cards_cubit.freezed.dart';

class UserCreditCardsCubit extends Cubit<UserCreditCardsState> {
  UserCreditCardsCubit() : super(UserCreditCardsState.initial());

  final dataProvider = ApiProvider(new http.Client());
  Future<void> fetchCreditCard() async {
    emit(UserCreditCardsState.loading());
    try {
      final list = await dataProvider.fetchCreditCard();
      emit(UserCreditCardsState.fetchSuccess(list));
    } catch (error) {
      emit(UserCreditCardsState.fetchFailed(error.toString()));
    }
  }
}
