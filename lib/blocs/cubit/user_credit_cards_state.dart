part of 'user_credit_cards_cubit.dart';

@freezed
abstract class UserCreditCardsState with _$UserCreditCardsState {
  const factory UserCreditCardsState.initial() = _Initial;
  const factory UserCreditCardsState.loading() = _Loading;
  const factory UserCreditCardsState.fetchSuccess(List<CreditCard> list) =
      _FetchSuccess;
  const factory UserCreditCardsState.fetchFailed(String error) = _FetchFailed;
}
