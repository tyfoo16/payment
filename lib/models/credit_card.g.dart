// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'credit_card.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CreditCard _$_$_CreditCardFromJson(Map<String, dynamic> json) {
  return _$_CreditCard(
    json['id'] as String,
    json['object'] as String? ?? '',
    json['address_city'] as String? ?? '',
    json['address_country'] as String? ?? '',
    json['address_line1'] as String? ?? '',
    json['address_line1_check'] as String? ?? '',
    json['address_line2'] as String? ?? '',
    json['address_state'] as String? ?? '',
    json['address_zip'] as String? ?? '',
    json['address_zip_check'] as String? ?? '',
    json['brand'] as String? ?? '',
    json['country'] as String? ?? '',
    json['customer'] as String? ?? '',
    json['cvc_check'] as String? ?? '',
    json['dynamic_last4'] as String? ?? '',
    json['exp_month'] as int,
    json['exp_year'] as int,
    json['fingerprint'] as String,
    json['funding'] as String,
    json['last4'] as String,
    json['metadata'] as Map<String, dynamic>,
    json['name'] as String? ?? '',
    json['tokenization_method'] as String? ?? '',
  );
}

Map<String, dynamic> _$_$_CreditCardToJson(_$_CreditCard instance) =>
    <String, dynamic>{
      'id': instance.id,
      'object': instance.object,
      'address_city': instance.addressCity,
      'address_country': instance.addressCountry,
      'address_line1': instance.addressLine1,
      'address_line1_check': instance.addressLine1Check,
      'address_line2': instance.addressLine2,
      'address_state': instance.addressState,
      'address_zip': instance.addressZip,
      'address_zip_check': instance.addressZipCheck,
      'brand': instance.brand,
      'country': instance.country,
      'customer': instance.customer,
      'cvc_check': instance.cvcCheck,
      'dynamic_last4': instance.dynamicLast4,
      'exp_month': instance.expMonth,
      'exp_year': instance.expYear,
      'fingerprint': instance.fingerprint,
      'funding': instance.funding,
      'last4': instance.last4,
      'metadata': instance.metadata,
      'name': instance.name,
      'tokenization_method': instance.tokenizationMethod,
    };
