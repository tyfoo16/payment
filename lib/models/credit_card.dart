import 'package:freezed_annotation/freezed_annotation.dart';

part 'credit_card.freezed.dart';
part 'credit_card.g.dart';

@freezed
class CreditCard with _$CreditCard {
  @JsonSerializable(explicitToJson: true)
  factory CreditCard(
    @JsonKey(name: 'id') String id,
    @JsonKey(name: 'object', defaultValue: '') String? object,
    @JsonKey(name: 'address_city', defaultValue: '') String? addressCity,
    @JsonKey(name: 'address_country', defaultValue: '') String? addressCountry,
    @JsonKey(name: 'address_line1', defaultValue: '') String? addressLine1,
    @JsonKey(name: 'address_line1_check', defaultValue: '')
        String? addressLine1Check,
    @JsonKey(name: 'address_line2', defaultValue: '') String? addressLine2,
    @JsonKey(name: 'address_state', defaultValue: '') String? addressState,
    @JsonKey(name: 'address_zip', defaultValue: '') String? addressZip,
    @JsonKey(name: 'address_zip_check', defaultValue: '')
        String? addressZipCheck,
    @JsonKey(name: 'brand', defaultValue: '') String brand,
    @JsonKey(name: 'country', defaultValue: '') String country,
    @JsonKey(name: 'customer', defaultValue: '') String customer,
    @JsonKey(name: 'cvc_check', defaultValue: '') String? cvcCheck,
    @JsonKey(name: 'dynamic_last4', defaultValue: '') String? dynamicLast4,
    @JsonKey(name: 'exp_month') int expMonth,
    @JsonKey(name: 'exp_year') int expYear,
    @JsonKey(name: 'fingerprint') String fingerprint,
    @JsonKey(name: 'funding') String funding,
    @JsonKey(name: 'last4') String last4,
    @JsonKey(name: 'metadata') Map<String, dynamic> metadata,
    @JsonKey(name: 'name', defaultValue: '') String? name,
    @JsonKey(name: 'tokenization_method', defaultValue: '')
        String? tokenizationMethod,
  ) = _CreditCard;

  factory CreditCard.fromJson(Map<String, dynamic> json) =>
      _$CreditCardFromJson(json);
}
