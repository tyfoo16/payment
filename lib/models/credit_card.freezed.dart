// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'credit_card.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CreditCard _$CreditCardFromJson(Map<String, dynamic> json) {
  return _CreditCard.fromJson(json);
}

/// @nodoc
class _$CreditCardTearOff {
  const _$CreditCardTearOff();

  _CreditCard call(
      @JsonKey(name: 'id')
          String id,
      @JsonKey(name: 'object', defaultValue: '')
          String? object,
      @JsonKey(name: 'address_city', defaultValue: '')
          String? addressCity,
      @JsonKey(name: 'address_country', defaultValue: '')
          String? addressCountry,
      @JsonKey(name: 'address_line1', defaultValue: '')
          String? addressLine1,
      @JsonKey(name: 'address_line1_check', defaultValue: '')
          String? addressLine1Check,
      @JsonKey(name: 'address_line2', defaultValue: '')
          String? addressLine2,
      @JsonKey(name: 'address_state', defaultValue: '')
          String? addressState,
      @JsonKey(name: 'address_zip', defaultValue: '')
          String? addressZip,
      @JsonKey(name: 'address_zip_check', defaultValue: '')
          String? addressZipCheck,
      @JsonKey(name: 'brand', defaultValue: '')
          String brand,
      @JsonKey(name: 'country', defaultValue: '')
          String country,
      @JsonKey(name: 'customer', defaultValue: '')
          String customer,
      @JsonKey(name: 'cvc_check', defaultValue: '')
          String? cvcCheck,
      @JsonKey(name: 'dynamic_last4', defaultValue: '')
          String? dynamicLast4,
      @JsonKey(name: 'exp_month')
          int expMonth,
      @JsonKey(name: 'exp_year')
          int expYear,
      @JsonKey(name: 'fingerprint')
          String fingerprint,
      @JsonKey(name: 'funding')
          String funding,
      @JsonKey(name: 'last4')
          String last4,
      @JsonKey(name: 'metadata')
          Map<String, dynamic> metadata,
      @JsonKey(name: 'name', defaultValue: '')
          String? name,
      @JsonKey(name: 'tokenization_method', defaultValue: '')
          String? tokenizationMethod) {
    return _CreditCard(
      id,
      object,
      addressCity,
      addressCountry,
      addressLine1,
      addressLine1Check,
      addressLine2,
      addressState,
      addressZip,
      addressZipCheck,
      brand,
      country,
      customer,
      cvcCheck,
      dynamicLast4,
      expMonth,
      expYear,
      fingerprint,
      funding,
      last4,
      metadata,
      name,
      tokenizationMethod,
    );
  }

  CreditCard fromJson(Map<String, Object> json) {
    return CreditCard.fromJson(json);
  }
}

/// @nodoc
const $CreditCard = _$CreditCardTearOff();

/// @nodoc
mixin _$CreditCard {
  @JsonKey(name: 'id')
  String get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'object', defaultValue: '')
  String? get object => throw _privateConstructorUsedError;
  @JsonKey(name: 'address_city', defaultValue: '')
  String? get addressCity => throw _privateConstructorUsedError;
  @JsonKey(name: 'address_country', defaultValue: '')
  String? get addressCountry => throw _privateConstructorUsedError;
  @JsonKey(name: 'address_line1', defaultValue: '')
  String? get addressLine1 => throw _privateConstructorUsedError;
  @JsonKey(name: 'address_line1_check', defaultValue: '')
  String? get addressLine1Check => throw _privateConstructorUsedError;
  @JsonKey(name: 'address_line2', defaultValue: '')
  String? get addressLine2 => throw _privateConstructorUsedError;
  @JsonKey(name: 'address_state', defaultValue: '')
  String? get addressState => throw _privateConstructorUsedError;
  @JsonKey(name: 'address_zip', defaultValue: '')
  String? get addressZip => throw _privateConstructorUsedError;
  @JsonKey(name: 'address_zip_check', defaultValue: '')
  String? get addressZipCheck => throw _privateConstructorUsedError;
  @JsonKey(name: 'brand', defaultValue: '')
  String get brand => throw _privateConstructorUsedError;
  @JsonKey(name: 'country', defaultValue: '')
  String get country => throw _privateConstructorUsedError;
  @JsonKey(name: 'customer', defaultValue: '')
  String get customer => throw _privateConstructorUsedError;
  @JsonKey(name: 'cvc_check', defaultValue: '')
  String? get cvcCheck => throw _privateConstructorUsedError;
  @JsonKey(name: 'dynamic_last4', defaultValue: '')
  String? get dynamicLast4 => throw _privateConstructorUsedError;
  @JsonKey(name: 'exp_month')
  int get expMonth => throw _privateConstructorUsedError;
  @JsonKey(name: 'exp_year')
  int get expYear => throw _privateConstructorUsedError;
  @JsonKey(name: 'fingerprint')
  String get fingerprint => throw _privateConstructorUsedError;
  @JsonKey(name: 'funding')
  String get funding => throw _privateConstructorUsedError;
  @JsonKey(name: 'last4')
  String get last4 => throw _privateConstructorUsedError;
  @JsonKey(name: 'metadata')
  Map<String, dynamic> get metadata => throw _privateConstructorUsedError;
  @JsonKey(name: 'name', defaultValue: '')
  String? get name => throw _privateConstructorUsedError;
  @JsonKey(name: 'tokenization_method', defaultValue: '')
  String? get tokenizationMethod => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CreditCardCopyWith<CreditCard> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CreditCardCopyWith<$Res> {
  factory $CreditCardCopyWith(
          CreditCard value, $Res Function(CreditCard) then) =
      _$CreditCardCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id')
          String id,
      @JsonKey(name: 'object', defaultValue: '')
          String? object,
      @JsonKey(name: 'address_city', defaultValue: '')
          String? addressCity,
      @JsonKey(name: 'address_country', defaultValue: '')
          String? addressCountry,
      @JsonKey(name: 'address_line1', defaultValue: '')
          String? addressLine1,
      @JsonKey(name: 'address_line1_check', defaultValue: '')
          String? addressLine1Check,
      @JsonKey(name: 'address_line2', defaultValue: '')
          String? addressLine2,
      @JsonKey(name: 'address_state', defaultValue: '')
          String? addressState,
      @JsonKey(name: 'address_zip', defaultValue: '')
          String? addressZip,
      @JsonKey(name: 'address_zip_check', defaultValue: '')
          String? addressZipCheck,
      @JsonKey(name: 'brand', defaultValue: '')
          String brand,
      @JsonKey(name: 'country', defaultValue: '')
          String country,
      @JsonKey(name: 'customer', defaultValue: '')
          String customer,
      @JsonKey(name: 'cvc_check', defaultValue: '')
          String? cvcCheck,
      @JsonKey(name: 'dynamic_last4', defaultValue: '')
          String? dynamicLast4,
      @JsonKey(name: 'exp_month')
          int expMonth,
      @JsonKey(name: 'exp_year')
          int expYear,
      @JsonKey(name: 'fingerprint')
          String fingerprint,
      @JsonKey(name: 'funding')
          String funding,
      @JsonKey(name: 'last4')
          String last4,
      @JsonKey(name: 'metadata')
          Map<String, dynamic> metadata,
      @JsonKey(name: 'name', defaultValue: '')
          String? name,
      @JsonKey(name: 'tokenization_method', defaultValue: '')
          String? tokenizationMethod});
}

/// @nodoc
class _$CreditCardCopyWithImpl<$Res> implements $CreditCardCopyWith<$Res> {
  _$CreditCardCopyWithImpl(this._value, this._then);

  final CreditCard _value;
  // ignore: unused_field
  final $Res Function(CreditCard) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? object = freezed,
    Object? addressCity = freezed,
    Object? addressCountry = freezed,
    Object? addressLine1 = freezed,
    Object? addressLine1Check = freezed,
    Object? addressLine2 = freezed,
    Object? addressState = freezed,
    Object? addressZip = freezed,
    Object? addressZipCheck = freezed,
    Object? brand = freezed,
    Object? country = freezed,
    Object? customer = freezed,
    Object? cvcCheck = freezed,
    Object? dynamicLast4 = freezed,
    Object? expMonth = freezed,
    Object? expYear = freezed,
    Object? fingerprint = freezed,
    Object? funding = freezed,
    Object? last4 = freezed,
    Object? metadata = freezed,
    Object? name = freezed,
    Object? tokenizationMethod = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      object: object == freezed
          ? _value.object
          : object // ignore: cast_nullable_to_non_nullable
              as String?,
      addressCity: addressCity == freezed
          ? _value.addressCity
          : addressCity // ignore: cast_nullable_to_non_nullable
              as String?,
      addressCountry: addressCountry == freezed
          ? _value.addressCountry
          : addressCountry // ignore: cast_nullable_to_non_nullable
              as String?,
      addressLine1: addressLine1 == freezed
          ? _value.addressLine1
          : addressLine1 // ignore: cast_nullable_to_non_nullable
              as String?,
      addressLine1Check: addressLine1Check == freezed
          ? _value.addressLine1Check
          : addressLine1Check // ignore: cast_nullable_to_non_nullable
              as String?,
      addressLine2: addressLine2 == freezed
          ? _value.addressLine2
          : addressLine2 // ignore: cast_nullable_to_non_nullable
              as String?,
      addressState: addressState == freezed
          ? _value.addressState
          : addressState // ignore: cast_nullable_to_non_nullable
              as String?,
      addressZip: addressZip == freezed
          ? _value.addressZip
          : addressZip // ignore: cast_nullable_to_non_nullable
              as String?,
      addressZipCheck: addressZipCheck == freezed
          ? _value.addressZipCheck
          : addressZipCheck // ignore: cast_nullable_to_non_nullable
              as String?,
      brand: brand == freezed
          ? _value.brand
          : brand // ignore: cast_nullable_to_non_nullable
              as String,
      country: country == freezed
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String,
      customer: customer == freezed
          ? _value.customer
          : customer // ignore: cast_nullable_to_non_nullable
              as String,
      cvcCheck: cvcCheck == freezed
          ? _value.cvcCheck
          : cvcCheck // ignore: cast_nullable_to_non_nullable
              as String?,
      dynamicLast4: dynamicLast4 == freezed
          ? _value.dynamicLast4
          : dynamicLast4 // ignore: cast_nullable_to_non_nullable
              as String?,
      expMonth: expMonth == freezed
          ? _value.expMonth
          : expMonth // ignore: cast_nullable_to_non_nullable
              as int,
      expYear: expYear == freezed
          ? _value.expYear
          : expYear // ignore: cast_nullable_to_non_nullable
              as int,
      fingerprint: fingerprint == freezed
          ? _value.fingerprint
          : fingerprint // ignore: cast_nullable_to_non_nullable
              as String,
      funding: funding == freezed
          ? _value.funding
          : funding // ignore: cast_nullable_to_non_nullable
              as String,
      last4: last4 == freezed
          ? _value.last4
          : last4 // ignore: cast_nullable_to_non_nullable
              as String,
      metadata: metadata == freezed
          ? _value.metadata
          : metadata // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      tokenizationMethod: tokenizationMethod == freezed
          ? _value.tokenizationMethod
          : tokenizationMethod // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$CreditCardCopyWith<$Res> implements $CreditCardCopyWith<$Res> {
  factory _$CreditCardCopyWith(
          _CreditCard value, $Res Function(_CreditCard) then) =
      __$CreditCardCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id')
          String id,
      @JsonKey(name: 'object', defaultValue: '')
          String? object,
      @JsonKey(name: 'address_city', defaultValue: '')
          String? addressCity,
      @JsonKey(name: 'address_country', defaultValue: '')
          String? addressCountry,
      @JsonKey(name: 'address_line1', defaultValue: '')
          String? addressLine1,
      @JsonKey(name: 'address_line1_check', defaultValue: '')
          String? addressLine1Check,
      @JsonKey(name: 'address_line2', defaultValue: '')
          String? addressLine2,
      @JsonKey(name: 'address_state', defaultValue: '')
          String? addressState,
      @JsonKey(name: 'address_zip', defaultValue: '')
          String? addressZip,
      @JsonKey(name: 'address_zip_check', defaultValue: '')
          String? addressZipCheck,
      @JsonKey(name: 'brand', defaultValue: '')
          String brand,
      @JsonKey(name: 'country', defaultValue: '')
          String country,
      @JsonKey(name: 'customer', defaultValue: '')
          String customer,
      @JsonKey(name: 'cvc_check', defaultValue: '')
          String? cvcCheck,
      @JsonKey(name: 'dynamic_last4', defaultValue: '')
          String? dynamicLast4,
      @JsonKey(name: 'exp_month')
          int expMonth,
      @JsonKey(name: 'exp_year')
          int expYear,
      @JsonKey(name: 'fingerprint')
          String fingerprint,
      @JsonKey(name: 'funding')
          String funding,
      @JsonKey(name: 'last4')
          String last4,
      @JsonKey(name: 'metadata')
          Map<String, dynamic> metadata,
      @JsonKey(name: 'name', defaultValue: '')
          String? name,
      @JsonKey(name: 'tokenization_method', defaultValue: '')
          String? tokenizationMethod});
}

/// @nodoc
class __$CreditCardCopyWithImpl<$Res> extends _$CreditCardCopyWithImpl<$Res>
    implements _$CreditCardCopyWith<$Res> {
  __$CreditCardCopyWithImpl(
      _CreditCard _value, $Res Function(_CreditCard) _then)
      : super(_value, (v) => _then(v as _CreditCard));

  @override
  _CreditCard get _value => super._value as _CreditCard;

  @override
  $Res call({
    Object? id = freezed,
    Object? object = freezed,
    Object? addressCity = freezed,
    Object? addressCountry = freezed,
    Object? addressLine1 = freezed,
    Object? addressLine1Check = freezed,
    Object? addressLine2 = freezed,
    Object? addressState = freezed,
    Object? addressZip = freezed,
    Object? addressZipCheck = freezed,
    Object? brand = freezed,
    Object? country = freezed,
    Object? customer = freezed,
    Object? cvcCheck = freezed,
    Object? dynamicLast4 = freezed,
    Object? expMonth = freezed,
    Object? expYear = freezed,
    Object? fingerprint = freezed,
    Object? funding = freezed,
    Object? last4 = freezed,
    Object? metadata = freezed,
    Object? name = freezed,
    Object? tokenizationMethod = freezed,
  }) {
    return _then(_CreditCard(
      id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      object == freezed
          ? _value.object
          : object // ignore: cast_nullable_to_non_nullable
              as String?,
      addressCity == freezed
          ? _value.addressCity
          : addressCity // ignore: cast_nullable_to_non_nullable
              as String?,
      addressCountry == freezed
          ? _value.addressCountry
          : addressCountry // ignore: cast_nullable_to_non_nullable
              as String?,
      addressLine1 == freezed
          ? _value.addressLine1
          : addressLine1 // ignore: cast_nullable_to_non_nullable
              as String?,
      addressLine1Check == freezed
          ? _value.addressLine1Check
          : addressLine1Check // ignore: cast_nullable_to_non_nullable
              as String?,
      addressLine2 == freezed
          ? _value.addressLine2
          : addressLine2 // ignore: cast_nullable_to_non_nullable
              as String?,
      addressState == freezed
          ? _value.addressState
          : addressState // ignore: cast_nullable_to_non_nullable
              as String?,
      addressZip == freezed
          ? _value.addressZip
          : addressZip // ignore: cast_nullable_to_non_nullable
              as String?,
      addressZipCheck == freezed
          ? _value.addressZipCheck
          : addressZipCheck // ignore: cast_nullable_to_non_nullable
              as String?,
      brand == freezed
          ? _value.brand
          : brand // ignore: cast_nullable_to_non_nullable
              as String,
      country == freezed
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String,
      customer == freezed
          ? _value.customer
          : customer // ignore: cast_nullable_to_non_nullable
              as String,
      cvcCheck == freezed
          ? _value.cvcCheck
          : cvcCheck // ignore: cast_nullable_to_non_nullable
              as String?,
      dynamicLast4 == freezed
          ? _value.dynamicLast4
          : dynamicLast4 // ignore: cast_nullable_to_non_nullable
              as String?,
      expMonth == freezed
          ? _value.expMonth
          : expMonth // ignore: cast_nullable_to_non_nullable
              as int,
      expYear == freezed
          ? _value.expYear
          : expYear // ignore: cast_nullable_to_non_nullable
              as int,
      fingerprint == freezed
          ? _value.fingerprint
          : fingerprint // ignore: cast_nullable_to_non_nullable
              as String,
      funding == freezed
          ? _value.funding
          : funding // ignore: cast_nullable_to_non_nullable
              as String,
      last4 == freezed
          ? _value.last4
          : last4 // ignore: cast_nullable_to_non_nullable
              as String,
      metadata == freezed
          ? _value.metadata
          : metadata // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>,
      name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      tokenizationMethod == freezed
          ? _value.tokenizationMethod
          : tokenizationMethod // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_CreditCard implements _CreditCard {
  _$_CreditCard(
      @JsonKey(name: 'id')
          this.id,
      @JsonKey(name: 'object', defaultValue: '')
          this.object,
      @JsonKey(name: 'address_city', defaultValue: '')
          this.addressCity,
      @JsonKey(name: 'address_country', defaultValue: '')
          this.addressCountry,
      @JsonKey(name: 'address_line1', defaultValue: '')
          this.addressLine1,
      @JsonKey(name: 'address_line1_check', defaultValue: '')
          this.addressLine1Check,
      @JsonKey(name: 'address_line2', defaultValue: '')
          this.addressLine2,
      @JsonKey(name: 'address_state', defaultValue: '')
          this.addressState,
      @JsonKey(name: 'address_zip', defaultValue: '')
          this.addressZip,
      @JsonKey(name: 'address_zip_check', defaultValue: '')
          this.addressZipCheck,
      @JsonKey(name: 'brand', defaultValue: '')
          this.brand,
      @JsonKey(name: 'country', defaultValue: '')
          this.country,
      @JsonKey(name: 'customer', defaultValue: '')
          this.customer,
      @JsonKey(name: 'cvc_check', defaultValue: '')
          this.cvcCheck,
      @JsonKey(name: 'dynamic_last4', defaultValue: '')
          this.dynamicLast4,
      @JsonKey(name: 'exp_month')
          this.expMonth,
      @JsonKey(name: 'exp_year')
          this.expYear,
      @JsonKey(name: 'fingerprint')
          this.fingerprint,
      @JsonKey(name: 'funding')
          this.funding,
      @JsonKey(name: 'last4')
          this.last4,
      @JsonKey(name: 'metadata')
          this.metadata,
      @JsonKey(name: 'name', defaultValue: '')
          this.name,
      @JsonKey(name: 'tokenization_method', defaultValue: '')
          this.tokenizationMethod);

  factory _$_CreditCard.fromJson(Map<String, dynamic> json) =>
      _$_$_CreditCardFromJson(json);

  @override
  @JsonKey(name: 'id')
  final String id;
  @override
  @JsonKey(name: 'object', defaultValue: '')
  final String? object;
  @override
  @JsonKey(name: 'address_city', defaultValue: '')
  final String? addressCity;
  @override
  @JsonKey(name: 'address_country', defaultValue: '')
  final String? addressCountry;
  @override
  @JsonKey(name: 'address_line1', defaultValue: '')
  final String? addressLine1;
  @override
  @JsonKey(name: 'address_line1_check', defaultValue: '')
  final String? addressLine1Check;
  @override
  @JsonKey(name: 'address_line2', defaultValue: '')
  final String? addressLine2;
  @override
  @JsonKey(name: 'address_state', defaultValue: '')
  final String? addressState;
  @override
  @JsonKey(name: 'address_zip', defaultValue: '')
  final String? addressZip;
  @override
  @JsonKey(name: 'address_zip_check', defaultValue: '')
  final String? addressZipCheck;
  @override
  @JsonKey(name: 'brand', defaultValue: '')
  final String brand;
  @override
  @JsonKey(name: 'country', defaultValue: '')
  final String country;
  @override
  @JsonKey(name: 'customer', defaultValue: '')
  final String customer;
  @override
  @JsonKey(name: 'cvc_check', defaultValue: '')
  final String? cvcCheck;
  @override
  @JsonKey(name: 'dynamic_last4', defaultValue: '')
  final String? dynamicLast4;
  @override
  @JsonKey(name: 'exp_month')
  final int expMonth;
  @override
  @JsonKey(name: 'exp_year')
  final int expYear;
  @override
  @JsonKey(name: 'fingerprint')
  final String fingerprint;
  @override
  @JsonKey(name: 'funding')
  final String funding;
  @override
  @JsonKey(name: 'last4')
  final String last4;
  @override
  @JsonKey(name: 'metadata')
  final Map<String, dynamic> metadata;
  @override
  @JsonKey(name: 'name', defaultValue: '')
  final String? name;
  @override
  @JsonKey(name: 'tokenization_method', defaultValue: '')
  final String? tokenizationMethod;

  @override
  String toString() {
    return 'CreditCard(id: $id, object: $object, addressCity: $addressCity, addressCountry: $addressCountry, addressLine1: $addressLine1, addressLine1Check: $addressLine1Check, addressLine2: $addressLine2, addressState: $addressState, addressZip: $addressZip, addressZipCheck: $addressZipCheck, brand: $brand, country: $country, customer: $customer, cvcCheck: $cvcCheck, dynamicLast4: $dynamicLast4, expMonth: $expMonth, expYear: $expYear, fingerprint: $fingerprint, funding: $funding, last4: $last4, metadata: $metadata, name: $name, tokenizationMethod: $tokenizationMethod)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CreditCard &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.object, object) ||
                const DeepCollectionEquality().equals(other.object, object)) &&
            (identical(other.addressCity, addressCity) ||
                const DeepCollectionEquality()
                    .equals(other.addressCity, addressCity)) &&
            (identical(other.addressCountry, addressCountry) ||
                const DeepCollectionEquality()
                    .equals(other.addressCountry, addressCountry)) &&
            (identical(other.addressLine1, addressLine1) ||
                const DeepCollectionEquality()
                    .equals(other.addressLine1, addressLine1)) &&
            (identical(other.addressLine1Check, addressLine1Check) ||
                const DeepCollectionEquality()
                    .equals(other.addressLine1Check, addressLine1Check)) &&
            (identical(other.addressLine2, addressLine2) ||
                const DeepCollectionEquality()
                    .equals(other.addressLine2, addressLine2)) &&
            (identical(other.addressState, addressState) ||
                const DeepCollectionEquality()
                    .equals(other.addressState, addressState)) &&
            (identical(other.addressZip, addressZip) ||
                const DeepCollectionEquality()
                    .equals(other.addressZip, addressZip)) &&
            (identical(other.addressZipCheck, addressZipCheck) ||
                const DeepCollectionEquality()
                    .equals(other.addressZipCheck, addressZipCheck)) &&
            (identical(other.brand, brand) ||
                const DeepCollectionEquality().equals(other.brand, brand)) &&
            (identical(other.country, country) ||
                const DeepCollectionEquality()
                    .equals(other.country, country)) &&
            (identical(other.customer, customer) ||
                const DeepCollectionEquality()
                    .equals(other.customer, customer)) &&
            (identical(other.cvcCheck, cvcCheck) ||
                const DeepCollectionEquality()
                    .equals(other.cvcCheck, cvcCheck)) &&
            (identical(other.dynamicLast4, dynamicLast4) ||
                const DeepCollectionEquality()
                    .equals(other.dynamicLast4, dynamicLast4)) &&
            (identical(other.expMonth, expMonth) ||
                const DeepCollectionEquality()
                    .equals(other.expMonth, expMonth)) &&
            (identical(other.expYear, expYear) ||
                const DeepCollectionEquality()
                    .equals(other.expYear, expYear)) &&
            (identical(other.fingerprint, fingerprint) ||
                const DeepCollectionEquality()
                    .equals(other.fingerprint, fingerprint)) &&
            (identical(other.funding, funding) ||
                const DeepCollectionEquality()
                    .equals(other.funding, funding)) &&
            (identical(other.last4, last4) ||
                const DeepCollectionEquality().equals(other.last4, last4)) &&
            (identical(other.metadata, metadata) ||
                const DeepCollectionEquality()
                    .equals(other.metadata, metadata)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.tokenizationMethod, tokenizationMethod) ||
                const DeepCollectionEquality()
                    .equals(other.tokenizationMethod, tokenizationMethod)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(object) ^
      const DeepCollectionEquality().hash(addressCity) ^
      const DeepCollectionEquality().hash(addressCountry) ^
      const DeepCollectionEquality().hash(addressLine1) ^
      const DeepCollectionEquality().hash(addressLine1Check) ^
      const DeepCollectionEquality().hash(addressLine2) ^
      const DeepCollectionEquality().hash(addressState) ^
      const DeepCollectionEquality().hash(addressZip) ^
      const DeepCollectionEquality().hash(addressZipCheck) ^
      const DeepCollectionEquality().hash(brand) ^
      const DeepCollectionEquality().hash(country) ^
      const DeepCollectionEquality().hash(customer) ^
      const DeepCollectionEquality().hash(cvcCheck) ^
      const DeepCollectionEquality().hash(dynamicLast4) ^
      const DeepCollectionEquality().hash(expMonth) ^
      const DeepCollectionEquality().hash(expYear) ^
      const DeepCollectionEquality().hash(fingerprint) ^
      const DeepCollectionEquality().hash(funding) ^
      const DeepCollectionEquality().hash(last4) ^
      const DeepCollectionEquality().hash(metadata) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(tokenizationMethod);

  @JsonKey(ignore: true)
  @override
  _$CreditCardCopyWith<_CreditCard> get copyWith =>
      __$CreditCardCopyWithImpl<_CreditCard>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_CreditCardToJson(this);
  }
}

abstract class _CreditCard implements CreditCard {
  factory _CreditCard(
      @JsonKey(name: 'id')
          String id,
      @JsonKey(name: 'object', defaultValue: '')
          String? object,
      @JsonKey(name: 'address_city', defaultValue: '')
          String? addressCity,
      @JsonKey(name: 'address_country', defaultValue: '')
          String? addressCountry,
      @JsonKey(name: 'address_line1', defaultValue: '')
          String? addressLine1,
      @JsonKey(name: 'address_line1_check', defaultValue: '')
          String? addressLine1Check,
      @JsonKey(name: 'address_line2', defaultValue: '')
          String? addressLine2,
      @JsonKey(name: 'address_state', defaultValue: '')
          String? addressState,
      @JsonKey(name: 'address_zip', defaultValue: '')
          String? addressZip,
      @JsonKey(name: 'address_zip_check', defaultValue: '')
          String? addressZipCheck,
      @JsonKey(name: 'brand', defaultValue: '')
          String brand,
      @JsonKey(name: 'country', defaultValue: '')
          String country,
      @JsonKey(name: 'customer', defaultValue: '')
          String customer,
      @JsonKey(name: 'cvc_check', defaultValue: '')
          String? cvcCheck,
      @JsonKey(name: 'dynamic_last4', defaultValue: '')
          String? dynamicLast4,
      @JsonKey(name: 'exp_month')
          int expMonth,
      @JsonKey(name: 'exp_year')
          int expYear,
      @JsonKey(name: 'fingerprint')
          String fingerprint,
      @JsonKey(name: 'funding')
          String funding,
      @JsonKey(name: 'last4')
          String last4,
      @JsonKey(name: 'metadata')
          Map<String, dynamic> metadata,
      @JsonKey(name: 'name', defaultValue: '')
          String? name,
      @JsonKey(name: 'tokenization_method', defaultValue: '')
          String? tokenizationMethod) = _$_CreditCard;

  factory _CreditCard.fromJson(Map<String, dynamic> json) =
      _$_CreditCard.fromJson;

  @override
  @JsonKey(name: 'id')
  String get id => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'object', defaultValue: '')
  String? get object => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'address_city', defaultValue: '')
  String? get addressCity => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'address_country', defaultValue: '')
  String? get addressCountry => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'address_line1', defaultValue: '')
  String? get addressLine1 => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'address_line1_check', defaultValue: '')
  String? get addressLine1Check => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'address_line2', defaultValue: '')
  String? get addressLine2 => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'address_state', defaultValue: '')
  String? get addressState => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'address_zip', defaultValue: '')
  String? get addressZip => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'address_zip_check', defaultValue: '')
  String? get addressZipCheck => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'brand', defaultValue: '')
  String get brand => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'country', defaultValue: '')
  String get country => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'customer', defaultValue: '')
  String get customer => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'cvc_check', defaultValue: '')
  String? get cvcCheck => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'dynamic_last4', defaultValue: '')
  String? get dynamicLast4 => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'exp_month')
  int get expMonth => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'exp_year')
  int get expYear => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'fingerprint')
  String get fingerprint => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'funding')
  String get funding => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'last4')
  String get last4 => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'metadata')
  Map<String, dynamic> get metadata => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'name', defaultValue: '')
  String? get name => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'tokenization_method', defaultValue: '')
  String? get tokenizationMethod => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$CreditCardCopyWith<_CreditCard> get copyWith =>
      throw _privateConstructorUsedError;
}
