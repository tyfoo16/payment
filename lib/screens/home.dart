import 'package:bjak/blocs/cubit/user_credit_cards_cubit.dart';
import 'package:bjak/models/credit_card.dart';
import 'package:bjak/widgets/alert_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    fetchCards();
  }

  void fetchCards() {
    BlocProvider.of<UserCreditCardsCubit>(context).fetchCreditCard();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(32),
          child: _buildCardList(),
        ),
      ),
    );
  }

  Widget _buildCardList() {
    return BlocConsumer<UserCreditCardsCubit, UserCreditCardsState>(
      listener: (context, state) {
        state.when(
          initial: () => null,
          loading: () => null,
          fetchSuccess: (_) => null,
          fetchFailed: (error) => showAlertDialog(
            context,
            "Fetch Error",
            error,
          ),
        );
      },
      builder: (context, state) {
        return state.when(
            initial: () => Container(),
            loading: () =>
                Container(child: Center(child: CircularProgressIndicator())),
            fetchSuccess: (list) {
              return Container(
                child: RefreshIndicator(
                  onRefresh: onRefresh,
                  child: ListView.builder(
                    itemCount: list.length,
                    itemBuilder: (context, i) {
                      return _buildCard(list[i]);
                    },
                  ),
                ),
              );
            },
            fetchFailed: (err) {
              return Container();
            });
      },
    );
  }

  Future<void> onRefresh() async {
    fetchCards();
    return;
  }

  Widget _buildCard(CreditCard card) {
    TextStyle _style = TextStyle(
      fontWeight: FontWeight.w600,
      fontSize: 16,
      color: Colors.white,
    );
    return AspectRatio(
      aspectRatio: 1.9,
      child: Container(
        margin: EdgeInsets.only(bottom: 16),
        padding: EdgeInsets.all(12),
        decoration: BoxDecoration(
          color: Colors.redAccent,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
              offset: Offset(8, 16),
              blurRadius: 30,
              spreadRadius: 4,
              color: Colors.black12.withOpacity(0.1),
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 20),
            Align(
              alignment: Alignment.center,
              child: Text(
                "XXXX XXXX XXXX XXXX",
                style:
                    _style.copyWith(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
            Expanded(child: Container()),
            Text(card.brand.toString(), style: _style),
            Text(card.expMonth.toString() + "/" + card.expYear.toString(),
                style: _style),
            Text(card.name!.isNotEmpty ? card.name! : "CARD HOLDER",
                style: _style),
          ],
        ),
      ),
    );
  }

  showDialog() {}
}
