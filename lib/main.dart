import 'package:bjak/blocs/cubit/user_credit_cards_cubit.dart';
import 'package:bjak/screens/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => UserCreditCardsCubit(),
      child: MaterialApp(
        home: HomeScreen(),
      ),
    );
  }
}
